<?php

namespace CustomIS\AuthBundle\Controller;

use CustomIS\AuthBundle\Form\LoginType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\Form\FormFactory;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\HttpFoundation\Session\Flash\FlashBagInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

/**
 * Class DefaultController
 */
class DefaultController
{
    /**
     * @Route("/login", name="login")
     *
     * @param FormFactoryInterface $formFactory
     * @param AuthenticationUtils  $authenticationUtils
     * @param FlashBagInterface    $flashBag
     *
     * @return array
     */
    public function loginAction(FormFactoryInterface $formFactory, AuthenticationUtils $authenticationUtils, FlashBagInterface $flashBag): array
    {
        $lastUsername = $authenticationUtils->getLastUsername();
        $error = $authenticationUtils->getLastAuthenticationError();

        $form = $formFactory->create(LoginType::class);
        $form->get('_username')->setData($lastUsername);

        if ($error instanceof AuthenticationException) {
            $flashBag->add('error', 'Došlo k chybě při přihlašování');
        }

        return [
            'form'     => $form->createView(),
            'fullPage' => true,
        ];
    }
}
