<?php

declare(strict_types = 1);

namespace CustomIS\AuthBundle\Ldap;

use FreeDSx\Ldap\LdapClient;

/**
 * Class LdapFactory
 */
class LdapFactory
{
    /**
     * @var string|string[]
     */
    private $server;

    /**
     * @var string
     */
    private $baseDn;

    /**
     * @var string
     */
    private $username;

    /**
     * @var string
     */
    private $password;

    /**
     * @var LdapClient|null
     */
    private $client;

    /**
     * LdapFactory constructor.
     *
     * @param string|string[] $server
     * @param string          $baseDn
     * @param string          $username
     * @param string          $password
     */
    public function __construct($server, string $baseDn, string $username, string $password)
    {
        $this->server = (array) $server;
        $this->baseDn = $baseDn;
        $this->username = $username;
        $this->password = $password;
    }

    /**
     * @return LdapClient
     */
    public function create(): LdapClient
    {
        if ($this->client === null) {
            $this->client = new LdapClient([
                'servers' => $this->server,
                'base_dn' => $this->baseDn,
            ]);

            $this->client->bind($this->username, $this->password);
        }

        return $this->client;
    }
}
